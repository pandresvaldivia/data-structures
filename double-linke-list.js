class Node {
	constructor(value) {
		this.value = value;
		this.next = null;
		this.prev = null;
	}
}

function defaultEquals(a, b) {
	return a === b;
}

class DoubleLinkedList {
	constructor() {
		this.head = null;
		this.tail = null;
		this.count = 0;
	}

	add(value) {
		const node = new Node(value);
		if (this.size() === 0) {
			this.head = node;
			this.tail = node;
		} else {
			node.prev = this.tail;
			this.tail.next = node;
			this.tail = node;
		}
		this.count++;
	}

	remove(value) {
		if (this.head == null) return false;

		let removedElement;
		if (value === this.head.value) {
			removedElement = this.head;
			if (this.head === this.tail) {
				this.head = null;
				this.tail = null;
			} else {
				this.head = this.head.next;
				this.head.prev = null;
			}

			return removedElement;
		}

		let current = this.head.next;

		while (current != null && value !== current.value) {
			current = current.next;
		}

		if (current == this.tail) {
			removedElement = this.tail;
			this.tail = this.tail.prev;
			this.tail.next = null;

			return removedElement;
		} else if (current != null) {
			removedElement = current;
			current.prev.next = current.next;
			current.next.prev = current.prev;
			return removedElement;
		}

		return undefined;
	}

	toString() {
		if (this.head == null) {
			return '';
		}

		let objString = `${this.head.value}`;
		let current = this.head.next;
		for (let i = 1; i < this.size() && current != null; i++) {
			objString = `${objString},${current.value}`;
			current = current.next;
		}
		return objString;
	}

	inverseToString() {
		if (this.tail == null) {
			return '';
		}
		let objString = `${this.tail.value}`;
		let previous = this.tail.prev;
		while (previous != null) {
			objString = `${objString},${previous.value}`;
			previous = previous.prev;
		}
		return objString;
	}

	getTail() {
		return this.tail;
	}

	size() {
		return this.count;
	}
}

const myDoubleLinkedList = new DoubleLinkedList();

myDoubleLinkedList.add(13);
console.log(myDoubleLinkedList.toString());
console.log(myDoubleLinkedList.inverseToString());
myDoubleLinkedList.add(12);
console.log(myDoubleLinkedList.toString());
console.log(myDoubleLinkedList.inverseToString());
myDoubleLinkedList.add(14);
console.log(myDoubleLinkedList.toString());
console.log(myDoubleLinkedList.inverseToString());
myDoubleLinkedList.add(15);
console.log(myDoubleLinkedList.toString());
console.log(myDoubleLinkedList.inverseToString());
// myDoubleLinkedList.remove(13);
// console.log(myDoubleLinkedList.toString());
// console.log(myDoubleLinkedList.inverseToString());
// myDoubleLinkedList.remove(15);
// console.log(myDoubleLinkedList.toString());
// console.log(myDoubleLinkedList.inverseToString());
myDoubleLinkedList.remove(14);
console.log(myDoubleLinkedList.toString());
console.log(myDoubleLinkedList.inverseToString());
