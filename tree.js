class Node {
	constructor(value) {
		this.value = value;
		this.left = null;
		this.right = null;
	}
}

class Tree {
	constructor() {
		this.root = null;
		this.count = 0;
	}

	insert(value) {
		const node = new Node(value);

		if (this.root === null) {
			this.root = node;
			this.count++;

			return;
		}

		this.insertNode(this.root, node);
	}

	insertNode(current, node) {
		if (node.value === current.value) return;

		if (node.value < current.value) {
			if (current.left === null) {
				current.left = node;
				this.count++;
				return;
			}

			this.insertNode(current.left, node);
			return;
		}

		if (current.right === null) {
			current.right = node;
			this.count++;
			return;
		}

		this.insertNode(current.right, node);
	}

	contains(value, current = this.root) {
		if (current === null) return false;

		if (current.value === value) return true;

		if (value < current.value) return this.contains(value, current.left);

		return this.contains(value, current.right);
	}

	findNode(value, current = this.root) {
		if (current === null) return null;

		if (value === current.value) return current;

		if (value < current.value) return this.findNode(value, current.left);

		return this.findNode(value, current.right);
	}

	findParent(value, current = this.root) {
		if (value === current.value) return null;

		if (value < current.value) {
			if (current.left === null) return null;

			if (current.left.value === value) return current;

			return this.findParent(value, current.left);
		}

		if (current.right === null) return null;

		if (current.right.value === value) return current;

		return this.findParent(value, current.right);
	}

	remove(value) {
		const nodeToRemove = this.findNode(value);

		if (nodeToRemove === null) return null;

		const parentNode = this.findParent(value);

		if (this.count === 1) {
			this.root = null;
			this.count = 0;
			return nodeToRemove;
		}

		this.count--;

		if (nodeToRemove.left === null && nodeToRemove.right === null) {
			if (parentNode.left.value === value) {
				parentNode.left = null;
				return nodeToRemove;
			}
			parentNode.right = null;
			return nodeToRemove;
		}

		if (nodeToRemove.left !== null && nodeToRemove.right !== null) {
			let next = nodeToRemove.right;

			while (next.left !== null) {
				next = next.left;
			}

			// Se reemplaza por el nodo menor de su
			// subtree derecho, ya que es el unico
			// capaz de balancear los subtrees
			if (nodeToRemove.right !== next) {
				this.remove(next.value);
				nodeToRemove.value = next.value;
				return nodeToRemove;
			}

			nodeToRemove.value = next.value;
			nodeToRemove.right = next.right;
			return nodeToRemove;
		}

		let next =
			nodeToRemove.left === null ? nodeToRemove.right : nodeToRemove.left;

		if (this.root === nodeToRemove) {
			this.root = next;
			return nodeToRemove;
		}

		if (parentNode.left === nodeToRemove) {
			parentNode.left = nodeToRemove;
			return nodeToRemove;
		}

		parentNode.right = nodeToRemove;
	}

	getDepth(current = this.root) {
		if (current === null) return 0;

		let leftDepth = this.getDepth(current.left);
		let rightDepth = this.getDepth(current.right);

		return leftDepth > rightDepth ? leftDepth + 1 : rightDepth + 1;
	}

	getMin(current = this.root) {
		if (current.left === null) return current.value;

		return this.getMin(current.left);
	}

	getMax(current = this.root) {
		if (current.right === null) return current.value;

		return this.getMax(current.right);
	}
}

const tree = new Tree();

tree.insert(60);
tree.insert(41);
tree.insert(74);
tree.insert(16);
tree.insert(53);
tree.insert(65);
tree.insert(25);
tree.insert(46);
tree.insert(55);
tree.insert(63);
tree.insert(70);
tree.insert(42);
tree.insert(56);
tree.insert(62);
tree.insert(64);

console.log('----- Tree depth -----');
console.log(tree.getDepth());

console.log('\n\n\n----- Tree min -----');
console.log(tree.getMin());

console.log('\n\n\n----- Tree max -----');
console.log(tree.getMax());

console.log('\n\n\n----- Tree -----');
console.log(tree);
console.log('\n\n--- Node 41 ---');
console.log(tree.findNode(41));
console.log('\n\n--- Node 74 ---');
console.log(tree.findNode(74));
console.log('\n\n--- Node 16 ---');
console.log(tree.findNode(16));
console.log('\n\n--- Node 53 ---');
console.log(tree.findNode(53));
console.log('\n\n--- Node 65 ---');
console.log(tree.findNode(65));
console.log('\n\n--- Node 25 ---');
console.log(tree.findNode(25));
console.log('\n\n--- Node 46 ---');
console.log(tree.findNode(46));
console.log('\n\n--- Node 55 ---');
console.log(tree.findNode(55));
console.log('\n\n--- Node 63 ---');
console.log(tree.findNode(63));
console.log('\n\n--- Node 70 ---');
console.log(tree.findNode(70));
console.log('\n\n--- Node 42 ---');
console.log(tree.findNode(42));
console.log('\n\n--- Node 56 ---');
console.log(tree.findNode(56));
console.log('\n\n--- Node 62 ---');
console.log(tree.findNode(62));
console.log('\n\n--- Node 64 ---');
console.log(tree.findNode(64));

console.log('\n\n\n\n----- REMOVE NODE ------');
console.log('\n\n----- REMOVE 41 ------');
console.log(tree.remove(41));
console.log(tree);

console.log('\n\n----- REMOVE 53 ------');
console.log(tree.remove(53));
console.log(tree.findNode(42));

console.log('\n\n----- REMOVE ROOT ------');
console.log(tree.remove(60));
console.log(tree);
