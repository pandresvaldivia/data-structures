const person = {
	nombre: 'Anderson',
	apellido: 'Gonzalez',

	saludar: function () {
		console.log(`Hola, me llamo ${this.nombre} ${this.apellido}`);
	},
};

class Person {
	constructor(_nombre, _apellido) {
		this.nombre = _nombre;
		this.apellido = _apellido;
	}

	saludar() {
		console.log('Hola');
	}
}

const persona1 = new Person('Nombre 1', 'Apellido 1');

console.log(persona1);
