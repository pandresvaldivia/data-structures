const { LinkedList } = require('./linked-list')

class Queue {
	constructor(){
		this.linkedList = new LinkedList();
		this.head = null;
		this.count = 0;
	}

	enqueue(value) {
		this.linkedList.add(value);
		this.head = this.linkedList.head;
	}

	dequeue() {
		if (this.head === null) return false;

		const value = this.head.value;

		if (this.head === this.tail) {
			this.head = null;
			this.tail = null;
		} else {
			this.head = this.head.next;
		}

		return value;
	}

	peek() {
		return this.head;
	}

	toString(){
		return this.linkedList.toString();
	}
}

const myQueue = new Queue();

myQueue.enqueue('Hola')
myQueue.enqueue('dos')
myQueue.enqueue(3)
myQueue.enqueue('four')
console.log(myQueue.toString())
console.log(myQueue.peek())
console.log(myQueue.toString())
console.log(myQueue.dequeue())
console.log(myQueue.toString())
console.log(myQueue.dequeue())
console.log(myQueue.toString())

module.exports = {
	Queue
}