const { LinkedList } = require('./linked-list')

class Stack {
    constructor(){
        this.linkedList = new LinkedList();
        this.head = null;
        this.count = 0;
    }

    push(value){
        this.linkedList.prepend(value);
        this.head = this.linkedList.head;
    }

    pop(){
        if (this.head === null) return false;

		const value = this.head.value;

		if (this.head === this.tail) {
			this.head = null;
			this.tail = null;
		} else {
			this.head = this.head.next;
		}

		return value;
    }

    peek(){
        return this.head;
    }

    toString(){
        return this.linkedList.toString();
    }
}

const myStack = new Stack();

myStack.push('Hola')
myStack.push('dos')
myStack.push(3)
myStack.push('four')
console.log(myStack.toString())
console.log(myStack.peek())
console.log(myStack.pop())
console.log(myStack.toString())