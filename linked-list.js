class Node {
	constructor(value) {
		this.value = value;
		this.next = null;
	}
}

class LinkedList {
	constructor() {
		this.head = null;
		this.tail = null;
		this.count = 0;
	}

	add(value) {
		const node = new Node(value);
		
		this.count++;

		if (this.head === null) {
			this.head = node;
			this.tail = node;

			return
		}
		
		this.tail.next = node;
		this.tail = node;
	}

	prepend(value) {
		const node = new Node(value);

		node.next = this.head;
		this.head = node;

		if (this.tail === null) {
			this.tail = node;
		}

		this.count++;
	}

	contains(value) {
		let current = this.head;

		while (current !== null && current.value !== value) {
			current = current.next;
		}

		return current !== null
	}

	indexOf(value){
		let current = this.head;

		for (let index = 0; index < this.size() && current !== null; index++) {
			if (current.value === value) {
				return index;
			}

			current = current.next;
		}

		return -1;
	}

	remove(value) {
		let current = this.head;

		if (current === null) return false;

		if (current.value === value) {
			if (this.head === this.tail) {
				this.head = null;
				this.tail = null;
			} else {
				this.head = current.next;
			}

			this.count--;
			return true;
		}

		while (current.next !== null && current.next.value !== value) {
			current = current.next;
		}

		if (current.next !== null) {
			if (current.next === this.tail) {
				current.next = null;
				this.tail = current;
			} else {
				current.next = current.next.next;
			}
			this.count--;

			return true;
		}

		return false;
	}

	size() {
		return this.count;
	}

	isEmpty() {
		return this.size() === 0;
	}

	toString() {
		if (this.head === null) return '';

		let text = `${this.head.value}`;
		let current = this.head.next;

		while (current !== null) {
			text += `, ${current.value}`;
			current = current.next;
		}

		return text;
	}
}

module.exports = {
	LinkedList
}